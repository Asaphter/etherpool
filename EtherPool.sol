// SPDX-License-Identifier: GPL-3.0
pragma solidity >=0.7.0 <0.9.0;

contract ETHPool {

    error NotEnoughFundsInContractBalance();
    error MinumumDonationIsOneWei();
    error MaximumWithdrawalIsOneEthPerWeek();
    error CantWithdrawMoreThanOneEthUntilNextReset();
    error LastResetLessThanOneWeekAgo();

    uint256 public resetInterval;
    struct weiRequest {
        uint256 amount;
        uint256 timestamp;
    }
    mapping(address => weiRequest) public withdrawalAddresses;
    
    
    constructor(uint256 theResetInterval){
        resetInterval = theResetInterval;
    }

    function donateWei() public payable {
        if(msg.value < 1)
            revert MinumumDonationIsOneWei();
    }

    function lastRequestInCurrentEpoch(weiRequest memory request) internal view returns (bool) {
        return (withdrawalAddresses[msg.sender].timestamp != 0 && block.timestamp - request.timestamp < resetInterval);
    }

    function getWei(uint256 weiAmount) public {
        if (weiAmount > 1 ether) 
            revert MaximumWithdrawalIsOneEthPerWeek();

        if (weiAmount > address(this).balance)
            revert NotEnoughFundsInContractBalance();

        if (lastRequestInCurrentEpoch(withdrawalAddresses[msg.sender])){
            if (withdrawalAddresses[msg.sender].amount + weiAmount > 1 ether)
                revert CantWithdrawMoreThanOneEthUntilNextReset();
            withdrawalAddresses[msg.sender].amount += weiAmount;
        }else{
            withdrawalAddresses[msg.sender].amount = weiAmount;
            withdrawalAddresses[msg.sender].timestamp = block.timestamp;
        }
        
        payable(msg.sender).transfer(weiAmount);
    }

}